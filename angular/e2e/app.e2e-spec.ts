import { EjemploTemplatePage } from './app.po';

describe('Ejemplo App', function() {
  let page: EjemploTemplatePage;

  beforeEach(() => {
    page = new EjemploTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
