import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { PersonaServiceProxy, ActualizarPersonaDTO } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';//https://stackoverflow.com/questions/35166168/how-to-use-moment-js-library-in-angular-2-typescript-app
import { MatSelectModule } from '@angular/material/select';

import { MatStepperModule } from '@angular/material/stepper';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'editar-persona-modal',
    templateUrl: './editar-persona.component.html',
    animations: [appModuleAnimation()],
    styleUrls: ['./editar-persona.component.css']
})
export class EditarPersonaComponent extends AppComponentBase implements OnInit {

    @ViewChild('editarPersonaModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    persona: ActualizarPersonaDTO = null;

    fechaInicio: Date;
    fechaFin: Date;

    constructor(
        injector: Injector,
        private _personaService: PersonaServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {

    }

    show(id: number): void {
        alert(JSON.stringify(id));
        this._personaService.get(id)
            .subscribe(
            (result) => {
                alert(JSON.stringify(result));
                this.persona = result;
                    this.active = true;
                    this.modal.show();
                }
            );
    }

    onShown(): void {
        $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        this.saving = true;
        this._personaService.update(this.persona)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
