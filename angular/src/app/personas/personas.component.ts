import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import {
    PersonaServiceProxy,
    PersonaDTO,
    PagedResultDtoOfPersonaDTO
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from 'shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { CrearPersonaComponent } from './crear-persona/crear-persona.component';
import { EditarPersonaComponent } from './editar-persona/editar-persona.component';
import { Moment } from 'moment';
@Component({
    selector: 'app-personas',
    templateUrl: './personas.component.html',
    animations: [appModuleAnimation()],
    styleUrls: ['./personas.component.css']
})
export class PersonasComponent extends PagedListingComponentBase<PersonaDTO>  {

    @ViewChild('crearPersonaModal') crearPersonaModal: CrearPersonaComponent;
    @ViewChild('editarPersonaModal') editarPersonaModal: EditarPersonaComponent;

    active: boolean = false;
    personas: PersonaDTO[] = [];

    constructor(injector: Injector,
        private _personaService: PersonaServiceProxy, private router: Router, private route: ActivatedRoute) {
        super(injector);
    }

    protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._personaService.getAll(100, 0)
            .pipe(finalize(() => {
                finishedCallback()
            }))
            .subscribe((result: PagedResultDtoOfPersonaDTO) => {
                alert(JSON.stringify(result.items));
                this.personas = result.items;
                this.showPaging(result, pageNumber);
            });
    }

    protected delete(persona: PersonaDTO): void {
        abp.message.confirm(
            "",
            String.fromCharCode(191) + "Esta seguro que desea borrar la persona " + persona.id + "?",
            (result: boolean) => {
                if (result) {
                    this._personaService.delete(persona.id)
                        .subscribe(() => {
                            abp.notify.info("Borrado exitosamente la persona: " + persona.id);
                            this.refresh();
                        });
                }
            }
        );
    }

    // Show Modals
    crearPersona(): void {
        this.crearPersonaModal.show();
    }

    editarPersona(persona: PersonaDTO): void {
        this.editarPersonaModal.show(persona.id);
    }

}
