import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { PersonaServiceProxy, CrearPersonaDTO } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';//https://stackoverflow.com/questions/35166168/how-to-use-moment-js-library-in-angular-2-typescript-app
import { MatSelectModule } from '@angular/material/select';

import { MatStepperModule } from '@angular/material/stepper';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { MatFormFieldModule } from '@angular/material/form-field';
export interface TipoIdentificacion {
    value: string;
    viewValue: string;
}

@Component({
    selector: 'crear-persona-modal',
    templateUrl: './crear-persona.component.html',
    animations: [appModuleAnimation()],
    styleUrls: ['./crear-persona.component.css']
})



export class CrearPersonaComponent extends AppComponentBase implements OnInit {


    @ViewChild('crearPersonaModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    persona: CrearPersonaDTO = new CrearPersonaDTO();
    listaTipoIdentificacion: TipoIdentificacion[] = [
        { value: 'CC', viewValue: 'CC' },
        { value: 'NIT', viewValue: 'NIT' },
        { value: 'No informa', viewValue: 'No informa' }
    ];

    selectedValue: string;
    fechaInicio: any;
    fechaFin: Date;
    formatoFecha: string = 'MM/DD/YYYY';
    //personaUtilitaria: PersonaUtilitaria;
    //listaSexo: Sexo[] = [
    //    { value: 'Masculino', viewValue: 'Masculino' },
    //    { value: 'Feminio', viewValue: 'Feminio' },
    //    { value: 'No informa', viewValue: 'No informa' }
    //];
    constructor(
        injector: Injector,
        private _personaServiceProxy: PersonaServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        alert(JSON.stringify(this.listaTipoIdentificacion));
        this.listaTipoIdentificacion;
        
    }

   

    show(): void {
        this.active = true;
        this.modal.show();
        // this.listarSexo();
        this.persona = new CrearPersonaDTO();
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
        //var _thisCrearPersona= this;


    }

    //https://github.com/angular/material2/issues/4615
    updateMyDate(idControl) {
        var inputConFecha = $("#" + idControl);
        inputConFecha.focus();
    }

    save(): void {
        this.saving = true;
        console.log(this.persona);
        alert(JSON.stringify(this.persona));
        
        this._personaServiceProxy.create(this.persona)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
