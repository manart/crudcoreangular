import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { PersonasComponent } from '@app/personas/personas.component';
import { CrearPersonaComponent } from '@app/personas/crear-persona/crear-persona.component';
import { EditarPersonaComponent } from '@app/personas/editar-persona/editar-persona.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent,  canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'personas', component: PersonasComponent, canActivate: [AppRouteGuard] },
                    { path: 'crear-persona', component: CrearPersonaComponent, canActivate: [AppRouteGuard] },
                    { path: 'editar-persona', component: EditarPersonaComponent, canActivate: [AppRouteGuard] }
               
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
