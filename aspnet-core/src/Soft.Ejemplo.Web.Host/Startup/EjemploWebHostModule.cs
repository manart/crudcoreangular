﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Soft.Ejemplo.Configuration;

namespace Soft.Ejemplo.Web.Host.Startup
{
    [DependsOn(
       typeof(EjemploWebCoreModule))]
    public class EjemploWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public EjemploWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(EjemploWebHostModule).GetAssembly());
        }
    }
}
