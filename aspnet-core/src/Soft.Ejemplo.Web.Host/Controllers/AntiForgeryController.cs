using Microsoft.AspNetCore.Antiforgery;
using Soft.Ejemplo.Controllers;

namespace Soft.Ejemplo.Web.Host.Controllers
{
    public class AntiForgeryController : EjemploControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
