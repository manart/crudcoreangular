﻿using Abp.AutoMapper;
using Soft.Ejemplo.Authentication.External;

namespace Soft.Ejemplo.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
