﻿using System.Collections.Generic;

namespace Soft.Ejemplo.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
