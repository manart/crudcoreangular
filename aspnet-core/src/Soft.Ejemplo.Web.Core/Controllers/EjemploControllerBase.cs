using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Soft.Ejemplo.Controllers
{
    public abstract class EjemploControllerBase: AbpController
    {
        protected EjemploControllerBase()
        {
            LocalizationSourceName = EjemploConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
