using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Soft.Ejemplo.EntityFrameworkCore
{
    public static class EjemploDbContextConfigurer
    {
        //public static void Configure(DbContextOptionsBuilder<EjemploDbContext> builder, string connectionString)
        //{
        //    builder.UseSqlServer(connectionString);
        //}

        //public static void Configure(DbContextOptionsBuilder<EjemploDbContext> builder, DbConnection connection)
        //{
        //    builder.UseSqlServer(connection);
        //}

        
            public static void Configure(DbContextOptionsBuilder<EjemploDbContext> builder, string connectionString)
            {
                builder.UseNpgsql(connectionString);
            }

            public static void Configure(DbContextOptionsBuilder<EjemploDbContext> builder, DbConnection connection)
            {
                builder.UseNpgsql(connection);
            }
        
    }
}
