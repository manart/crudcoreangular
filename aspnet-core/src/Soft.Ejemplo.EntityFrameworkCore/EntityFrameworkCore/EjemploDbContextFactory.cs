﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Soft.Ejemplo.Configuration;
using Soft.Ejemplo.Web;

namespace Soft.Ejemplo.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class EjemploDbContextFactory : IDesignTimeDbContextFactory<EjemploDbContext>
    {
        public EjemploDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<EjemploDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            EjemploDbContextConfigurer.Configure(builder, configuration.GetConnectionString(EjemploConsts.ConnectionStringName));

            return new EjemploDbContext(builder.Options);
        }
    }
}
