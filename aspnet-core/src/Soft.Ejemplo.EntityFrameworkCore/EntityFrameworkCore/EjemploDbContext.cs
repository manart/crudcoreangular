﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Soft.Ejemplo.Authorization.Roles;
using Soft.Ejemplo.Authorization.Users;
using Soft.Ejemplo.MultiTenancy;
using Soft.Ejemplo.Personas;

namespace Soft.Ejemplo.EntityFrameworkCore
{
    public class EjemploDbContext : AbpZeroDbContext<Tenant, Role, User, EjemploDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public DbSet<Persona> Persona { get; set; }
        public EjemploDbContext(DbContextOptions<EjemploDbContext> options)
            : base(options)
        {
        }
    }
}
