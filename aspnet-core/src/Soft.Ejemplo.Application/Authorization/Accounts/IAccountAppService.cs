﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Soft.Ejemplo.Authorization.Accounts.Dto;

namespace Soft.Ejemplo.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
