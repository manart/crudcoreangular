﻿using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;

namespace Soft.Ejemplo.Authorization.Accounts.Dto
{
    public class IsTenantAvailableInput
    {
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        public string TenancyName { get; set; }
    }
}
