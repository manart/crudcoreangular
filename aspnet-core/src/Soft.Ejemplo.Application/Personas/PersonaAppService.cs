﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Soft.Ejemplo.Personas.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Soft.Ejemplo.Personas
{
    public class PersonaAppService : AsyncCrudAppService<Persona, PersonaDTO, long, PagedResultRequestDto, CrearPersonaDTO, ActualizarPersonaDTO, EntityDto<long>, BorrarPersonaDTO>, IPersonaAppService
    {
        private readonly IPersonaDomainService _PersonaDomainService;


        public PersonaAppService(IRepository<Persona, long> repository, IPersonaDomainService PersonaDomainService) : base(repository)
        {
            _PersonaDomainService = PersonaDomainService;
        }
    }
 
}
