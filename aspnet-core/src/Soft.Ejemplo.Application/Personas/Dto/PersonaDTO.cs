﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Soft.Ejemplo.Personas.Dto
{
    public class PersonaDTO : EntityDto<long>
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public string TipoDocumento { get; set; }
        public string Identificacion { get; set; }
        public string Telefono { get; set; }
    }
}
