﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Soft.Ejemplo.Personas.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Soft.Ejemplo.Personas
{
    public interface IPersonaAppService : IAsyncCrudAppService<PersonaDTO, long, PagedResultRequestDto, CrearPersonaDTO, ActualizarPersonaDTO, EntityDto<long>, BorrarPersonaDTO>
    {
    }
    
}
