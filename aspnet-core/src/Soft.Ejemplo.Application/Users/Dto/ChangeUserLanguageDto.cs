using System.ComponentModel.DataAnnotations;

namespace Soft.Ejemplo.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}