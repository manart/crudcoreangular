using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Soft.Ejemplo.Roles.Dto;
using Soft.Ejemplo.Users.Dto;

namespace Soft.Ejemplo.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
