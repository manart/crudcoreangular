﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Soft.Ejemplo.Configuration.Dto;

namespace Soft.Ejemplo.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : EjemploAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
