﻿using System.Threading.Tasks;
using Soft.Ejemplo.Configuration.Dto;

namespace Soft.Ejemplo.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
