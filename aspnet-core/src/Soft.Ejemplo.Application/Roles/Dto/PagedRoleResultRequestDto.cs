﻿using Abp.Application.Services.Dto;

namespace Soft.Ejemplo.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

