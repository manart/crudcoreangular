﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Soft.Ejemplo.MultiTenancy.Dto;

namespace Soft.Ejemplo.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

