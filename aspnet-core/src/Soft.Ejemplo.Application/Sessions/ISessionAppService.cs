﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Soft.Ejemplo.Sessions.Dto;

namespace Soft.Ejemplo.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
