﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Soft.Ejemplo.Authorization;

namespace Soft.Ejemplo
{
    [DependsOn(
        typeof(EjemploCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class EjemploApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<EjemploAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(EjemploApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
