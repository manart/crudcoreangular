﻿using AutoMapper;
using Soft.Ejemplo.Personas;
using Soft.Ejemplo.Personas.Dto;
using System;
using System.Collections.Generic;
using System.Text;


namespace Soft.Ejemplo.AutoMapProfiles
{
    public class PersonaProfile : Profile
    {

        public PersonaProfile()
        {
            CreateMap<PersonaDTO, Persona>().ReverseMap();
            CreateMap<CrearPersonaDTO, Persona>().ReverseMap();
            CreateMap<ActualizarPersonaDTO, Persona>().ReverseMap();

        }
    }
}
