﻿using Abp.MultiTenancy;
using Soft.Ejemplo.Authorization.Users;

namespace Soft.Ejemplo.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
