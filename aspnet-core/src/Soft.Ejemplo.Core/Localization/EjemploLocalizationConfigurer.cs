﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace Soft.Ejemplo.Localization
{
    public static class EjemploLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(EjemploConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(EjemploLocalizationConfigurer).GetAssembly(),
                        "Soft.Ejemplo.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
