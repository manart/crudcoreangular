﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Soft.Ejemplo.Personas
{
    public class Persona : FullAuditedEntity<long>
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public string TipoDocumento { get; set; }
        public string Identificacion { get; set; }
        public string Telefono { get; set; }
    }
}
