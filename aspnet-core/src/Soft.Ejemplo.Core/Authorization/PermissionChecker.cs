﻿using Abp.Authorization;
using Soft.Ejemplo.Authorization.Roles;
using Soft.Ejemplo.Authorization.Users;

namespace Soft.Ejemplo.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
