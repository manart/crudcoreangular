﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Soft.Ejemplo.Authorization.Personas
{
    public class PersonaDomainService : IPersonaDomainService
    {
        private IRepository<Persona, long> _PersonaRepository;


        public PersonaDomainService(IRepository<Persona, long> PersonaRepository)
        {
            _PersonaRepository = PersonaRepository;
        }
    }
}
