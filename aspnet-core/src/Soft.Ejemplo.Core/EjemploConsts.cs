﻿namespace Soft.Ejemplo
{
    public class EjemploConsts
    {
        public const string LocalizationSourceName = "Ejemplo";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
